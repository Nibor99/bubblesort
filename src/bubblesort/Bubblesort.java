/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bubblesort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author rbeer
 */
public class Bubblesort {

    static int[] list = new int[50];
   
    public static void main(String[] args) {
        Random r = new Random();
        for(int i=0; i<list.length-1; i++){
            list[i]= (int) (Math.random()*100000);
        }
        bsort(list);
    }

    private static void bsort(int[] list) {
       for(int i=list.length-1; i>0; i--){
           for(int j=0; j<i; j++){
               if(list[j]>list[j+1]){
                   int temp = list[j]; //zwischenspeichern
                   list[j] = list[j+1];
                   list[j+1] = temp;
               }
           }
       }
        System.out.println(Arrays.toString(list));
    }

}
